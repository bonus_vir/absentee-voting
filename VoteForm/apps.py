from django.apps import AppConfig


class VoteformConfig(AppConfig):
    name = 'VoteForm'
